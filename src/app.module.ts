import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import UrlCollectorModule from './url-collector/url-collector.module';
import { envsSchema } from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: envsSchema,
    }),
    UrlCollectorModule,
  ],
})
export default class AppModule {}
