export default class CollectUrlsWithinDateRangeCommand {
  constructor(public readonly startDate: Date, public readonly endDate: Date) {}
}
