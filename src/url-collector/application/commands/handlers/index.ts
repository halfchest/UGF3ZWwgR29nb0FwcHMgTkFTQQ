import CollectUrlsWithinDateRangeCommandHandler from './collect-urls-within-date-range.command-handler';

const commandHandlers = [CollectUrlsWithinDateRangeCommandHandler];

export default commandHandlers;
