import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import CollectUrlsWithinDateRangeCommand from '../collect-urls-within-date-range.command';
import UrlsService from '../../../domain/services/urls.service';
import DateRange from '../../../domain/models/date-range';
import CollectUrlsResponse from '../../../api/dtos/responses/collect-urls.response';
import ConcurrencyLockService from '../../../domain/services/concurrency-lock.service';

@CommandHandler(CollectUrlsWithinDateRangeCommand)
export default class CollectUrlsWithinDateRangeCommandHandler
  implements
    ICommandHandler<CollectUrlsWithinDateRangeCommand, CollectUrlsResponse>
{
  constructor(
    public readonly urlsService: UrlsService,
    public readonly concurrencyLockService: ConcurrencyLockService,
  ) {}

  async execute(
    command: CollectUrlsWithinDateRangeCommand,
  ): Promise<CollectUrlsResponse> {
    const dateRange = DateRange.create(command.startDate, command.endDate);
    await this.concurrencyLockService.lock();
    const urls = await this.urlsService.fetchUrls(dateRange);
    this.concurrencyLockService.unlock();

    return { urls };
  }
}
