export default interface CollectUrlsResponse {
  urls: string[];
}
