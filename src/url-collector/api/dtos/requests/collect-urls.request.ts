import { IsDateString } from 'class-validator';

export default class CollectUrlsRequest {
  @IsDateString()
  start_date: Date;

  @IsDateString()
  end_date: Date;
}
