import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import DomainError from '../../domain/errors/domain-error';

type InputValidationError = Error & {
  response: { message: string | string[] };
};

@Catch()
export default class HttpExceptionsMapper extends BaseExceptionFilter {
  catch(exception: Error, host: ArgumentsHost) {
    Logger.error(exception.stack);

    if (exception instanceof DomainError) {
      return super.catch(
        new BadRequestException({
          error: `Error[code: ${exception.code}] occurred: ${exception.message}`,
        }),
        host,
      );
    }

    if (this.isInputValidationError(exception)) {
      const message = Array.isArray(exception.response.message)
        ? exception.response.message.join(', ')
        : exception.response.message;
      return super.catch(
        new BadRequestException({
          error: `Input validation error: ${message}`,
        }),
        host,
      );
    }

    super.catch(
      new InternalServerErrorException({ error: exception?.message }),
      host,
    );
  }

  private isInputValidationError(
    exception: Error,
  ): exception is InputValidationError {
    return (
      'response' in exception &&
      typeof exception.response === 'object' &&
      'message' in exception.response &&
      (typeof exception.response.message === 'string' ||
        (Array.isArray(exception.response.message) &&
          exception.response.message.every((item) => typeof item === 'string')))
    );
  }
}
