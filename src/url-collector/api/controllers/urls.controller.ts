import { Controller, Get, Query } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import CollectUrlsWithinDateRangeCommand from '../../application/commands/collect-urls-within-date-range.command';
import CollectUrlsResponse from '../dtos/responses/collect-urls.response';
import CollectUrlsRequest from '../dtos/requests/collect-urls.request';

@Controller('pictures')
export default class UrlsController {
  constructor(private readonly commandBus: CommandBus) {}

  @Get()
  collectUrls(
    @Query() { start_date, end_date }: CollectUrlsRequest,
  ): Promise<CollectUrlsResponse> {
    return this.commandBus.execute(
      new CollectUrlsWithinDateRangeCommand(
        new Date(start_date),
        new Date(end_date),
      ),
    );
  }
}
