import UrlsService from '../../domain/services/urls.service';
import NasaUrlsService from './nasa-urls.service';
import InmemoryConcurrencyLockService from './inmemory-concurrency-lock.service';
import ConcurrencyLockService from '../../domain/services/concurrency-lock.service';

const services = [
  { provide: UrlsService, useClass: NasaUrlsService },
  { provide: ConcurrencyLockService, useClass: InmemoryConcurrencyLockService },
];

export default services;
