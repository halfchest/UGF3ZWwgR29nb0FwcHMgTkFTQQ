import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import ConcurrencyLockService from '../../domain/services/concurrency-lock.service';
import { Env } from '../../../config';

@Injectable()
export default class InmemoryConcurrencyLockService
  implements ConcurrencyLockService
{
  private locksCount = 0;

  private readonly maxLocks: number;

  constructor(configService: ConfigService) {
    this.maxLocks = configService.get<number>(Env.CONCURRENT_REQUESTS);
  }

  async lock(): Promise<void> {
    if (this.locksCount >= this.maxLocks) {
      await this.wait(200);
      return this.lock();
    }

    this.locksCount += 1;
  }

  private wait(milliseconds: number) {
    return new Promise((resolve) =>
      setTimeout(() => resolve(true), milliseconds),
    );
  }

  unlock(): void {
    this.locksCount -= 1;
  }
}
