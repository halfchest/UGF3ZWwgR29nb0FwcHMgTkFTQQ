import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom, map } from 'rxjs';
import { ConfigService } from '@nestjs/config';
import UrlsService from '../../domain/services/urls.service';
import DateRange from '../../domain/models/date-range';
import Url from '../../domain/models/url';
import { Env } from '../../../config';

type NasaGetAPODResponse = {
  url: string;
}[];

@Injectable()
export default class NasaUrlsService implements UrlsService {
  private static BASE_URL = 'https://api.nasa.gov/planetary/apod';

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  fetchUrls(dateRange: DateRange): Promise<Url[]> {
    const nasaApiUrl = this.buildUrl(dateRange);
    return firstValueFrom(
      this.httpService
        .get<NasaGetAPODResponse>(nasaApiUrl)
        .pipe(map(({ data }) => data.map(({ url }) => url as Url))),
    );
  }

  private buildUrl(dateRange: DateRange): string {
    const apiKey = this.configService.get<string>(Env.API_KEY);
    const startDate = dateRange.fromAsIsoString;
    const endDate = dateRange.toAsIsoString;
    return `${NasaUrlsService.BASE_URL}?api_key=${apiKey}&start_date=${startDate}&end_date=${endDate}`;
  }
}
