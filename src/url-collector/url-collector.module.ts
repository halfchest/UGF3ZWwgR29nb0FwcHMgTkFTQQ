import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { HttpModule } from '@nestjs/axios';
import { APP_FILTER } from '@nestjs/core';
import commandHandlers from './application/commands/handlers';
import services from './infrastructure/services';
import controllers from './api/controllers';
import HttpExceptionsMapper from './api/middlewares/http-exception-mapper';

@Module({
  imports: [HttpModule, CqrsModule],
  controllers: [...controllers],
  providers: [
    ...commandHandlers,
    ...services,

    {
      provide: APP_FILTER,
      useClass: HttpExceptionsMapper,
    },
  ],
})
export default class UrlCollectorModule {}
