import DateRange from '../models/date-range';
import Url from '../models/url';

export default abstract class UrlsService {
  abstract fetchUrls(dateRange: DateRange): Promise<Url[]>;
}
