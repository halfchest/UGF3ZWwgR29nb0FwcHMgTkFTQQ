export default abstract class ConcurrencyLockService {
  abstract lock(): Promise<void>;
  abstract unlock(): void;
}
