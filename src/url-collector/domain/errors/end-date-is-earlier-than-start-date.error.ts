import DomainError from './domain-error';

export default class EndDateIsEarlierThanStartDateError extends DomainError {
  readonly code = 10001;

  constructor() {
    super(`End date cannot be earlier than start date!`);
  }
}
