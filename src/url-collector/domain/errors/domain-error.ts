export default abstract class DomainError extends Error {
  abstract readonly code: number;

  protected constructor(message: string) {
    super(message);
  }
}
