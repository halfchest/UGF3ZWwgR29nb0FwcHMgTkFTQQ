declare const Nominal: unique symbol;

type NominalType<TType, TNominal extends string> = TType & {
  [Nominal]: TNominal;
};

export default NominalType;
