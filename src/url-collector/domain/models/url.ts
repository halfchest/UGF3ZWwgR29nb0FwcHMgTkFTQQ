import NominalType from './nominal';

type Url = NominalType<string, 'url'>;

export default Url;
