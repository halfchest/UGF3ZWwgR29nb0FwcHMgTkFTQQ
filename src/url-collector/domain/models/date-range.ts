import EndDateIsEarlierThanStartDateError from '../errors/end-date-is-earlier-than-start-date.error';

export default class DateRange {
  static create(from: Date, to: Date) {
    if (from > to) {
      throw new EndDateIsEarlierThanStartDateError();
    }
    return new DateRange(from, to);
  }

  constructor(public readonly from: Date, public readonly to: Date) {}

  get fromAsIsoString(): string {
    return this.mapDateToISOStringWithoutTime(this.from);
  }

  get toAsIsoString(): string {
    return this.mapDateToISOStringWithoutTime(this.to);
  }

  private mapDateToISOStringWithoutTime(date: Date): string {
    return date.toISOString().split('T')[0];
  }
}
