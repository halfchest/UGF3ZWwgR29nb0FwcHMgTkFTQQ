import * as Joi from 'joi';

enum Env {
  API_KEY = 'API_KEY',
  CONCURRENT_REQUESTS = 'CONCURRENT_REQUESTS',
  PORT = 'PORT',
}

const envsSchema = Joi.object({
  [Env.API_KEY]: Joi.string().default('DEMO_KEY'),
  [Env.CONCURRENT_REQUESTS]: Joi.number().default(5),
  [Env.PORT]: Joi.number().default(8080),
});

export { Env, envsSchema };
