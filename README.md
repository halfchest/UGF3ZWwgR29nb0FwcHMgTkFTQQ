## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod

# docker
$ docker build -t url-collector . 
$ docker run -p 8080:8080 url-collector

```

## Test

```bash
# unit-test test
$ yarn run test

# e2e test
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```
