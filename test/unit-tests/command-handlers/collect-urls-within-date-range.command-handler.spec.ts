import { Test, TestingModule } from '@nestjs/testing';
import { faker } from '@faker-js/faker';
import CollectUrlsWithinDateRangeCommandHandler from '../../../src/url-collector/application/commands/handlers/collect-urls-within-date-range.command-handler';
import UrlsService from '../../../src/url-collector/domain/services/urls.service';
import ConcurrencyLockService from '../../../src/url-collector/domain/services/concurrency-lock.service';
import CollectUrlsWithinDateRangeCommand from '../../../src/url-collector/application/commands/collect-urls-within-date-range.command';
import EndDateIsEarlierThanStartDateError from '../../../src/url-collector/domain/errors/end-date-is-earlier-than-start-date.error';

describe('CollectUrlsWithinDateRangeCommandHandler', () => {
  let handler: CollectUrlsWithinDateRangeCommandHandler;
  const URLS_MOCK = [
    faker.internet.url(),
    faker.internet.url(),
    faker.internet.url(),
  ];

  beforeEach(async () => {
    const UrlsServiceProvider = {
      provide: UrlsService,
      useValue: {
        fetchUrls: jest.fn(() => URLS_MOCK),
      },
    };
    const ConcurrencyLockServiceProvider = {
      provide: ConcurrencyLockService,
      useValue: {
        lock: jest.fn(),
        unlock: jest.fn(),
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CollectUrlsWithinDateRangeCommandHandler,

        // dependencies
        UrlsServiceProvider,
        ConcurrencyLockServiceProvider,
      ],
    }).compile();

    handler = module.get<CollectUrlsWithinDateRangeCommandHandler>(
      CollectUrlsWithinDateRangeCommandHandler,
    );
  });

  it('should return correct CollectUrlsResponse', async () => {
    // given
    const startDate = new Date('2022-12-21');
    const endDate = new Date('2022-12-22');
    const command = new CollectUrlsWithinDateRangeCommand(startDate, endDate);

    // when
    const response = await handler.execute(command);

    // then
    expect(response).toMatchObject({
      urls: URLS_MOCK,
    });
  });

  it('should throws error when startDate is after endDate', async () => {
    // given
    const startDate = new Date('2022-12-22');
    const endDate = new Date('2022-12-21');
    const command = new CollectUrlsWithinDateRangeCommand(startDate, endDate);

    // when
    const execution = () => handler.execute(command);

    // then
    await expect(execution).rejects.toThrow(EndDateIsEarlierThanStartDateError);
  });
});
