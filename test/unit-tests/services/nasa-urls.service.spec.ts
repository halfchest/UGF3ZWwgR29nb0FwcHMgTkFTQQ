import { Test, TestingModule } from '@nestjs/testing';
import { faker } from '@faker-js/faker';
import { HttpService } from '@nestjs/axios';
import { of } from 'rxjs';
import { ConfigService } from '@nestjs/config';
import NasaUrlsService from '../../../src/url-collector/infrastructure/services/nasa-urls.service';
import CollectUrlsWithinDateRangeCommandHandler from '../../../src/url-collector/application/commands/handlers/collect-urls-within-date-range.command-handler';
import { Env } from '../../../src/config';
import DateRange from '../../../src/url-collector/domain/models/date-range';

describe('CollectUrlsWithinDateRangeCommandHandler', () => {
  let service: NasaUrlsService;
  let httpServiceMock: HttpService;
  const API_KEY = 'DEMO_API_KEY';
  const GET_APOD_RESPONSE_MOCK = {
    data: [
      { url: faker.internet.url() },
      { url: faker.internet.url() },
      { url: faker.internet.url() },
    ],
  };

  beforeEach(async () => {
    const HttpServiceProvider = {
      provide: HttpService,
      useValue: {
        get: jest.fn(() => of(GET_APOD_RESPONSE_MOCK)),
      },
    };
    const ConfigServiceProvider = {
      provide: ConfigService,
      useValue: {
        get: jest.fn((envName) => (envName === Env.API_KEY ? API_KEY : null)),
      },
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NasaUrlsService,

        // dependencies
        HttpServiceProvider,
        ConfigServiceProvider,
      ],
    }).compile();

    service = module.get<NasaUrlsService>(NasaUrlsService);
    httpServiceMock = module.get<HttpService>(HttpService);
  });

  it('should send GET request on correct url', async () => {
    // given
    const startDate = '2022-12-21';
    const endDate = '2022-12-22';
    const dateRange = DateRange.create(new Date(startDate), new Date(endDate));

    // when
    await service.fetchUrls(dateRange);

    // then
    const expectedUrl = `https://api.nasa.gov/planetary/apod?api_key=DEMO_API_KEY&start_date=${startDate}&end_date=${endDate}`;
    expect(httpServiceMock.get).toHaveBeenCalledWith(expectedUrl);
  });

  it('should map response correctly', async () => {
    // given
    const startDate = new Date('2022-12-21');
    const endDate = new Date('2022-12-22');
    const dateRange = DateRange.create(startDate, endDate);

    // when
    const response = await service.fetchUrls(dateRange);

    // then
    const expectedUrls = GET_APOD_RESPONSE_MOCK.data.map(({ url }) => url);
    expect(response).toMatchObject(expectedUrls);
  });
});
