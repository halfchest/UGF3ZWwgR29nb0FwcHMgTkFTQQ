import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { HttpService } from '@nestjs/axios';
import { faker } from '@faker-js/faker';
import { of } from 'rxjs';
import AppModule from '../../src/app.module';

describe('E2E test', () => {
  const GET_APOD_RESPONSE_MOCK = {
    data: [
      { url: faker.internet.url() },
      { url: faker.internet.url() },
      { url: faker.internet.url() },
    ],
  };
  const URLS_MOCK = GET_APOD_RESPONSE_MOCK.data.map(({ url }) => url);

  let app: INestApplication;

  beforeAll(async () => {
    const HttpServiceMock = { get: jest.fn(() => of(GET_APOD_RESPONSE_MOCK)) };
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(HttpService)
      .useValue(HttpServiceMock)
      .compile();

    app = moduleFixture.createNestApplication();
    await app
      .useGlobalPipes(
        new ValidationPipe({
          transform: true,
          stopAtFirstError: true,
        }),
      )
      .init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('UrlsController', () => {
    it('/pictures (GET) should return correct response', () => {
      return request(app.getHttpServer())
        .get('/pictures?start_date=2022-12-21&end_date=2022-12-22')
        .expect(200)
        .expect({ urls: URLS_MOCK });
    });

    it('/pictures (GET) should return error when param start_date is missing', () => {
      return request(app.getHttpServer())
        .get('/pictures?end_date=2022-12-22')
        .expect(400)
        .expect({
          error:
            'Input validation error: start_date must be a valid ISO 8601 date string',
        });
    });

    it('/pictures (GET) should return error when param end_date is missing', () => {
      return request(app.getHttpServer())
        .get('/pictures?start_date=2022-12-22')
        .expect(400)
        .expect({
          error:
            'Input validation error: end_date must be a valid ISO 8601 date string',
        });
    });

    it('/pictures (GET) should return error when param start_date is after end_date', () => {
      return request(app.getHttpServer())
        .get('/pictures?start_date=2022-12-23&end_date=2022-12-22')
        .expect(400)
        .expect({
          error:
            'Error[code: 10001] occurred: End date cannot be earlier than start date!',
        });
    });
  });
});
